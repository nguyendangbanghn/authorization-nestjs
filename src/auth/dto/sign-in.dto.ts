import { IsNotEmpty, MinLength, MaxLength, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SignInDto {
  @ApiProperty({
    example: 'john',
    uniqueItems: true,
    minLength: 2,
    maxLength: 255,
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  @MaxLength(255)
  // @IsEmail()
  readonly username: string;

  @ApiProperty({
    example: 'changeme',
    description: 'The password of the User',
    format: 'string',
    minLength: 2,
    maxLength: 25,
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  @MaxLength(25)
  readonly password: string;
}
