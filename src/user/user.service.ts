import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ERROR, MyException } from 'src/common/exception';
import { User } from './schema/user.schema';
@Injectable()
export class UserService {
  public async isEmailUnique(email: string): Promise<boolean> {
    const user = await this.userModel.exists({ email });
    if (user) throw new MyException(ERROR.EMAIL_EXISTS);
    return true;
  }

  private readonly users = [
    {
      userId: 1,
      username: 'john',
      password: 'changeme',
    },
    {
      userId: 2,
      username: 'maria',
      password: 'guess',
    },
  ];

  async findOne(username: string): Promise<any> {
    return this.users.find((user) => user.username === username);
  }

  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}
}
