import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { GENDER, ROLE, STATUS } from 'src/common/enums';
import * as validator from 'validator';

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
  _id: string;

  @Prop({
    type: String,
    lowercase: true,
    validate: validator.isEmail,
    maxlength: 255,
    minlength: 6,
    required: [true, 'EMAIL_IS_REQUIRED'],
    unique: true,
  })
  email: string;

  @Prop({
    type: String,
  })
  phoneNumber: string;

  @Prop({
    type: String,
  })
  avatar: string;

  @Prop({
    type: String,
  })
  username: string;

  @Prop({
    type: String,
    minlength: 5,
    maxlength: 255,
    required: [true, 'PASSWORD_IS_REQUIRED'],
  })
  password: string;

  @Prop({
    type: String,
  })
  codeResetPassword: string;

  @Prop({ type: Number, enum: GENDER })
  gender: number;

  @Prop({
    type: [Number],
    enum: ROLE,
    default: ROLE.USER,
    required: [true, 'ROLE_IS_REQUIRED'],
  })
  roles: number[];

  @Prop({
    type: Number,
    enum: STATUS,
    default: STATUS.DEACTIVE,
    required: [true, 'STATUS_IS_REQUIRED'],
  })
  status: number;
}

export const UserSchema = SchemaFactory.createForClass(User);
