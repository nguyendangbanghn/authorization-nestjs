import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './schema/user.schema';
import { UserService } from './user.service';
import * as bcrypt from 'bcrypt';
@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: User.name,
        inject: [ConfigService],
        useFactory: async (configService: ConfigService) => {
          const schema = UserSchema;
          UserSchema.pre('save', async function (next) {
            try {
              // hash password
              if (!this.isModified('password')) {
                return next();
              }
              const hashed = await bcrypt.hash(
                this['password'],
                configService.get('authConfig.salt'),
              );
              this['password'] = hashed;
              return next();
            } catch (err) {
              return next(err);
            }
          });

          UserSchema.set('toJSON', {
            transform: function (doc, ret, opt) {
              try {
                delete ret['password'];
                delete ret['resetPasswordToken'];
              } catch (e) {}
              return ret;
            },
          });
          return schema;
        },
      },
    ]),
  ],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
