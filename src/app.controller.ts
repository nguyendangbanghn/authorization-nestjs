import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Public } from './common/decorators/puplic.decorator';

@ApiTags('Test server')
@Controller()
export class AppController {
  @Public()
  @Get()
  getHello(): string {
    return 'Server run success!';
  }
}
