import { HttpStatus } from '@nestjs/common';

export const ERROR = {
  EMAIL_EXISTS: {
    message: 'Email must be unique.',
  },
  EMAIL_NOT_FOUND: {
    message: 'Email not found.',
  },
  USER_NOT_FOUND: {
    message: 'User not found.',
    statusCode: HttpStatus.FORBIDDEN,
  },
  USER_IS_ACTIVE: {
    message: 'user is active.',
  },
  USER_IS_DEACTIVE: {
    message: 'user is deactive.',
    statusCode: HttpStatus.FORBIDDEN,
  },
  USER_IS_DELETE: {
    message: 'user is delete.',
    statusCode: HttpStatus.FORBIDDEN,
  },
  WRONG_EMAIL_OR_PASSWORD: {
    message: 'Wrong email or password.',
    statusCode: HttpStatus.FORBIDDEN,
  },
  INVALID_TOKEN: {
    message: 'Invalid token.',
  },
  SEND_MAIL_ERROR: {
    message: 'Send mail error',
  },
  CODE_RESET_PASSWORD_NOT_MATCH: {
    message: 'Code reset password not match!',
  },
};

let code = 1000;
for (const key in ERROR) {
  ERROR[key].errorCode = code;
  ERROR[key].statusCode = ERROR[key].statusCode || HttpStatus.BAD_REQUEST;
  code++;
}
