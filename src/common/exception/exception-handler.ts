import { HttpException } from '@nestjs/common';
type CustomError = {
  message: string;
  errorCode?: number;
  statusCode?: number;
};
export class MyException extends HttpException {
  constructor(err: CustomError) {
    super(err, err.statusCode || 400);
  }
}
