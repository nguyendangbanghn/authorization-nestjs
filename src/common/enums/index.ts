export enum GENDER {
  OTHER,
  MALE,
  FEMALE,
}
export enum ROLE {
  ADMIN,
  USER,
}
export enum STATUS {
  DELETE,
  DEACTIVE,
  ACTIVE,
}
export enum ROLE_PROJECT {
  ADMIN,
  CSKH,
}
