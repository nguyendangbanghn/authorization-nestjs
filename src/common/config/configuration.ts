const configuration = () => ({
  constant: {
    IS_PUBLIC_KEY: 'isPublic',
  },
  emailConfig: {
    activeEmailLife: 86400 * 3 * 1000, // 3 ngày,
    activeEmailSecret: 'Ftech-ai-active-email',
    urlActiveEmail: `${process.env.CLIENT_URL}/auth/active-account`,
    urlChangePassword: '',
    service: 'gmail',
    host: 'smtp.gmail.com',
    user: 'va.crm.ftech@gmail.com',
    pass: 'Vbn693178',
    port: undefined,
    secure: undefined,
  },
  authConfig: {
    secret: 'Ftech-ai',
    refreshTokenSecret: 'Ftech-ai-refresh-token',
    tokenLife: 86400, // 5 ngày
    refreshTokenLife: 86400 * 5, // 5 ngày
    salt: 8,
    urlChangePassword: `${process.env.CLIENT_URL}/auth/change-password`,
  },
  dbConfig: {
    url: process.env.MONGODB_URL,
  },
});

export default configuration;
